using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using AuditAPI.Contexts;

var builder = WebApplication.CreateBuilder(args);

//Add connection string
var connectionString = builder.Configuration.GetConnectionString("WebApiDatabase");

//Add context to the services collection
builder.Services.AddDbContext<AuditContext>(options =>
      options.UseNpgsql(connectionString));

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddAutoMapper(typeof(Program)); 

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddControllers().AddJsonOptions(x =>
    x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AuditAPI v1"));
    
    // Инициализация таблиц в БД
    using var serviceScope = app.Services.CreateScope();
    serviceScope.ServiceProvider.GetService<AuditContext>()?.Database.Migrate();

    HttpClientHandler clientHandler = new HttpClientHandler();
    clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

    // Pass the handler to httpclient(from you are calling api)
    HttpClient client = new HttpClient(clientHandler);
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
