using AuditAPI.Contexts;
using AuditAPI.DTOs;
using AuditAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuditAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppointmentsController : ControllerBase
    {
        private readonly AuditContext _context;

        public AppointmentsController(AuditContext context)
        {
            _context = context;
        }

        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AppointmentsDto>>> GetAppointments()
        {
            if (_context.Appointments == null)
            {
                return NotFound();
            }

            return await _context.Appointments
                .Select(x => ItemToDto(x))
                .ToListAsync();
        }

        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AppointmentsDto>> GetAppointment(long? id)
        {
            if (_context.Appointments == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointments.FindAsync(id);

            if (appointment == null)
            {
                return NotFound();
            }

            return ItemToDto(appointment);
        }

        // POST: api/TodoItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Appointments>> PostAppointment(AppointmentsDto appointmentsDto)
        {
            if (_context.Rooms == null)
            {
                return NotFound();
            }
            
            var room = await 
                _context
                    .Rooms
                    .FindAsync(appointmentsDto.RoomId);
            
            var appointment = new Appointments
            {
                Description = appointmentsDto.Description,
                Room = room,
                RoomId = appointmentsDto.RoomId
            };
            // if (_context.TodoItems == null)
            // {
            //     return Problem("Entity set 'TodoContext.TodoItems'  is null.");
            // }
            _context.Appointments.Add(appointment);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(
                nameof(GetAppointment),
                new { id = appointment.Id },
                ItemToDto(appointment)
            );
        }

        // PUT: api/TodoItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAppointment(long id, AppointmentsDto appointmentsDto)
        {
            if (id != appointmentsDto.Id)
            {
                return BadRequest();
            }

            var appointment = await _context.Appointments.FindAsync(id);
            if (appointment == null)
            {
                return NotFound();
            }

            appointment.Description = appointmentsDto.Description;
            appointment.Room = appointmentsDto.Room;
            appointment.RoomId = appointmentsDto.RoomId;

            _context.Entry(appointment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!AppointmentsExist(id))
            {
                return NotFound();
            }

            return CreatedAtAction(
                nameof(GetAppointment),
                new { id = appointment.Id },
                ItemToDto(appointment)
            );
        }

        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAppointment(long id)
        {
            if (_context.Appointments == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointments.FindAsync(id);
            if (appointment == null)
            {
                return NotFound();
            }

            _context.Appointments.Remove(appointment);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool AppointmentsExist(long id)
        {
            return (_context.Appointments?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        private static AppointmentsDto ItemToDto(Appointments appointment) =>
            new AppointmentsDto
            {
                Id = appointment.Id,
                Description = appointment.Description,
            };
    }
}