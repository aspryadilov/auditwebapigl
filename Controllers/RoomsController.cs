using AuditAPI.Contexts;
using AuditAPI.DTOs;
using AuditAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuditAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomsController : ControllerBase
    {
        private readonly AuditContext _context;

        public RoomsController(AuditContext context)
        {
            _context = context;
        }

        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoomsDTO>>> GetRooms()
        {
            if (_context.Rooms == null)
            {
                return NotFound();
            }

            return await _context.Rooms
                .Select(x => ItemToDto(x))
                .ToListAsync();
        }

        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RoomsDTO>> GetRooms(long id)
        {
            if (_context.Rooms == null)
            {
                return NotFound();
            }

            var room = await _context.Rooms.FindAsync(id);

            if (room == null)
            {
                return NotFound();
            }

            return ItemToDto(room);
        }

        // POST: api/TodoItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Rooms>> PostRooms(RoomsDTO roomsDto)
        {
            // if (_context.Appointments == null ||
                // _context.RoomTypes == null ||
                // _context.Departments == null ||
                // _context.BuildingItems == null)
            // {
                // return NotFound();
            // }

            // var appointment = await 
                // _context
                    // .Appointments
                    // .FindAsync(roomsDto.AppointmentId);
            /*
            var roomType = await 
                _context
                    .RoomTypes
                    .FindAsync(roomsDto.RoomTypesId);
            var department = await 
                _context
                    .Departments
                    .FindAsync(roomsDto.DepartmentId);
            var buildingItem = await _context
                .BuildingItems
                .FindAsync(roomsDto.BuildingItemId);
            */
            var room = new Rooms
            {
                Name = roomsDto.Name,
                AppointmentId = roomsDto.AppointmentId,
                // Appointment = appointment,
                RoomTypesId = roomsDto.RoomTypesId,
                // RoomType = roomType,
                DepartmentId = roomsDto.DepartmentId,
                // Department = department,
                Square = roomsDto.Square,
                Capacity = roomsDto.Capacity,
                Floors = roomsDto.Floors,
                Number = roomsDto.Number
            };
            // if (_context.TodoItems == null)
            // {
            //     return Problem("Entity set 'TodoContext.TodoItems'  is null.");
            // }
            _context.Rooms.Add(room);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(
                nameof(GetRooms),
                new { id = room.Id },
                ItemToDto(room)
            );
        }
        
        // PUT: api/TodoItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRooms(long id, RoomsDTO roomsDto)
        {
            if (id != roomsDto.Id)
            {
                return BadRequest();
            }

            var room = await _context.Rooms.FindAsync(id);
            if (room == null)
            {
                return NotFound();
            }

            room.Appointment = roomsDto.Appointment;
            room.AppointmentId = roomsDto.AppointmentId;
            room.Capacity = roomsDto.Capacity;
            room.Name = roomsDto.Name;
            room.Number = roomsDto.Number;
            // room.Plan = roomsDto.Plan;
            room.Square = roomsDto.Square;
            room.RoomType = roomsDto.RoomType;
            room.Floors = roomsDto.Floors;

            _context.Entry(room).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!RoomsExists(id))
            {
                return NotFound();
            }

            return CreatedAtAction(
            nameof(GetRooms),
            new { id = room.Id },
            ItemToDto(room)
            );
        }

        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRooms(long id)
        {
            if (_context.Rooms == null)
            {
                return NotFound();
            }

            var room = await _context.Rooms.FindAsync(id);
            if (room == null)
            {
                return NotFound();
            }

            _context.Rooms.Remove(room);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool RoomsExists(long id)
        {
            return (_context.Rooms?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        private static RoomsDTO ItemToDto(Rooms room) =>
            new RoomsDTO
            {
                Name = room.Name,
                AppointmentId = room.AppointmentId,
                Appointment = room.Appointment,
                RoomTypesId = room.RoomTypesId,
                RoomType = room.RoomType,
                Square = room.Square,
                // Plan = room.Plan,
                Capacity = room.Capacity,
                Floors = room.Floors,
                Number = room.Number
            };
    }
}