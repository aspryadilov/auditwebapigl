using AuditAPI.Contexts;
using AuditAPI.DTOs;
using AuditAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuditAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileItemController : ControllerBase
    {
        private readonly AuditContext _context;
        private readonly IWebHostEnvironment _appEnvironment;

        public FileItemController(AuditContext context, IWebHostEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FileItemDto>>> GetFileItems()
        {
            if (_context.File == null)
            {
                return NotFound();
            }

            return await _context.File
                .Select(x => ItemToDto(x))
                .ToListAsync();
        }

        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FileItemDto>> GetFileItem(long id)
        {
            if (_context.File == null)
            {
                return NotFound();
            }

            var fileItem = await _context.File.FindAsync(id);

            if (fileItem == null)
            {
                return NotFound();
            }

            return ItemToDto(fileItem);
        }

        // POST: api/TodoItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<FileItem>> PostFileItem(FileItemDto fileItemDto)
        {
            var fileItem = new FileItem
            {
                Name = fileItemDto.Name,
                Path = fileItemDto.Path
            };
            // if (_context.TodoItems == null)
            // {
            //     return Problem("Entity set 'TodoContext.TodoItems'  is null.");
            // }
            _context.File.Add(fileItem);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(
                nameof(GetFileItem),
                new { id = fileItem.Id },
                ItemToDto(fileItem)
            );
        }
        
        /*
        TODO Добавить маппинг, а так же понять как указывать тип файла, который принимается и сохраняется
        */
        [HttpPost("/addFile")]
        public async Task<IActionResult> AddFile(IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                // путь к папке Files
                string path = "/Files/" + uploadedFile.FileName;
                
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }
                
                FileItem file = new FileItem { Name = uploadedFile.FileName, Path = path };
                _context.File.Add(file);
                await _context.SaveChangesAsync();
                
                return Ok();
            }

            return Problem("Объект файла пуст");
        }

        // PUT: api/TodoItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRoomType(long id, FileItemDto fileItemDto)
        {
            if (id != fileItemDto.Id)
            {
                return BadRequest();
            }

            var fileItem = await _context.File.FindAsync(id);
            if (fileItem == null)
            {
                return NotFound();
            }

            fileItem.Name = fileItemDto.Name;
            fileItem.Path = fileItemDto.Path;

            _context.Entry(fileItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!FileItemExists(id))
            {
                return NotFound();
            }

            return CreatedAtAction(
                nameof(GetFileItem),
                new { id = fileItem.Id },
                ItemToDto(fileItem)
            );
        }

        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRoomType(long id)
        {
            if (_context.RoomTypes == null)
            {
                return NotFound();
            }

            var room = await _context.RoomTypes.FindAsync(id);
            if (room == null)
            {
                return NotFound();
            }

            _context.RoomTypes.Remove(room);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool FileItemExists(long id)
        {
            return (_context.File?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        private static FileItemDto ItemToDto(FileItem fileItem) =>
            new FileItemDto
            {
                Id = fileItem.Id,
                Path = fileItem.Path,
                RoomId = fileItem.RoomId,
                Rooms = fileItem.Rooms
            };
    }
}