using AuditAPI.Contexts;
using AuditAPI.DTOs;
using AuditAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuditAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        private readonly AuditContext _context;

        public DepartmentsController(AuditContext context)
        {
            _context = context;
        }

        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DepartmentsDto>>> GetDepartments()
        {
            if (_context.Departments == null)
            {
                return NotFound();
            }

            return await _context.Departments
                .Select(x => ItemToDto(x))
                .ToListAsync();
        }

        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DepartmentsDto>> GetDepartment(long id)
        {
            if (_context.Departments == null)
            {
                return NotFound();
            }

            var dep = await _context.Departments.FindAsync(id);

            if (dep == null)
            {
                return NotFound();
            }

            return ItemToDto(dep);
        }

        // POST: api/TodoItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Departments>> PostDepartment(DepartmentsDto depDto)
        {
            var dep = new Departments
            {
                Name = depDto.Name,
            };
            // if (_context.TodoItems == null)
            // {
            //     return Problem("Entity set 'TodoContext.TodoItems'  is null.");
            // }
            _context.Departments.Add(dep);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(
                nameof(GetDepartment),
                new { id = dep.Id },
                ItemToDto(dep)
            );
        }

        // PUT: api/TodoItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDepartment(long id, DepartmentsDto depDto)
        {
            if (id != depDto.Id)
            {
                return BadRequest();
            }

            var dep = await _context.Departments.FindAsync(id);
            if (dep == null)
            {
                return NotFound();
            }

            dep.Name = depDto.Name;

            _context.Entry(dep).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!DepartmentExists(id))
            {
                return NotFound();
            }

            return CreatedAtAction(
                nameof(GetDepartment),
                new { id = dep.Id },
                ItemToDto(dep)
            );
        }

        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDepartment(long id)
        {
            if (_context.Departments == null)
            {
                return NotFound();
            }

            var dep = await _context.Departments.FindAsync(id);
            if (dep == null)
            {
                return NotFound();
            }

            _context.Departments.Remove(dep);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool DepartmentExists(long id)
        {
            return (_context.Departments?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        private static DepartmentsDto ItemToDto(Departments departments) =>
            new DepartmentsDto
            {
                Id = departments.Id,
                Name = departments.Name,
            };
    }
}