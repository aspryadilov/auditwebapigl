using AuditAPI.Contexts;
using AuditAPI.DTOs;
using AuditAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuditAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuildingItemsController : ControllerBase
    {
        private readonly AuditContext _context;

        public BuildingItemsController(AuditContext context)
        {
            _context = context;
        }

        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BuildingItemDto>>> GetBuildingItems()
        {
            if (_context.BuildingItems == null)
            {
                return NotFound();
            }

            return await _context.BuildingItems
                .Select(x => ItemToDto(x))
                .ToListAsync();
        }

        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BuildingItemDto>> GetBuildingItem(long id)
        {
            if (_context.BuildingItems == null)
            {
                return NotFound();
            }

            var buildingItem = await _context.BuildingItems.FindAsync(id);

            if (buildingItem == null)
            {
                return NotFound();
            }

            return ItemToDto(buildingItem);
        }

        // POST: api/TodoItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BuildingItem>> PostBuildingItem(BuildingItemDto buildingItemDto)
        {
            var buildingItem = new BuildingItem
            {
                Name = buildingItemDto.Name,
                Address = buildingItemDto.Address,
                NumOfStoreys = buildingItemDto.NumOfStoreys
            };
            // if (_context.TodoItems == null)
            // {
            //     return Problem("Entity set 'TodoContext.TodoItems'  is null.");
            // }
            _context.BuildingItems.Add(buildingItem);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(
                nameof(GetBuildingItem),
                new { id = buildingItem.Id },
                ItemToDto(buildingItem)
            );
        }

        // PUT: api/TodoItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBuildingItem(long id, BuildingItemDto buildingItemDto)
        {
            if (id != buildingItemDto.Id)
            {
                return BadRequest();
            }

            var buildingItem = await _context.BuildingItems.FindAsync(id);
            if (buildingItem == null)
            {
                return NotFound();
            }

            buildingItem.Name = buildingItemDto.Name;
            buildingItem.Address = buildingItemDto.Address;
            buildingItem.NumOfStoreys = buildingItemDto.NumOfStoreys;

            _context.Entry(buildingItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!BuildingItemExists(id))
            {
                return NotFound();
            }

            return CreatedAtAction(
                nameof(GetBuildingItem),
                new { id = buildingItem.Id },
                ItemToDto(buildingItem)
            );
        }

        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBuildingItem(long id)
        {
            if (_context.BuildingItems == null)
            {
                return NotFound();
            }

            var buildingItem = await _context.BuildingItems.FindAsync(id);
            if (buildingItem == null)
            {
                return NotFound();
            }

            _context.BuildingItems.Remove(buildingItem);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool BuildingItemExists(long id)
        {
            return (_context.BuildingItems?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        private static BuildingItemDto ItemToDto(BuildingItem buildingItem) =>
            new BuildingItemDto
            {
                Id = buildingItem.Id,
                Name = buildingItem.Name,
                Address = buildingItem.Address,
                NumOfStoreys = buildingItem.NumOfStoreys
            };
    }
}