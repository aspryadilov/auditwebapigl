using AuditAPI.Contexts;
using AuditAPI.DTOs;
using AuditAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuditAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomTypesController : ControllerBase
    {
        private readonly AuditContext _context;

        public RoomTypesController(AuditContext context)
        {
            _context = context;
        }

        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoomTypesDto>>> GetRoomTypes()
        {
            if (_context.RoomTypes == null)
            {
                return NotFound();
            }

            return await _context.RoomTypes
                .Select(x => ItemToDto(x))
                .ToListAsync();
        }

        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RoomTypesDto>> GetRoomType(long id)
        {
            if (_context.RoomTypes == null)
            {
                return NotFound();
            }

            var roomType = await _context.RoomTypes.FindAsync(id);

            if (roomType == null)
            {
                return NotFound();
            }

            return ItemToDto(roomType);
        }

        // POST: api/TodoItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<RoomTypes>> PostRoomType(RoomTypesDto roomTypesDto)
        {
            var roomType = new RoomTypes
            {
                Description = roomTypesDto.Description,
            };
            // if (_context.TodoItems == null)
            // {
            //     return Problem("Entity set 'TodoContext.TodoItems'  is null.");
            // }
            _context.RoomTypes.Add(roomType);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(
                nameof(GetRoomType),
                new { id = roomType.Id },
                ItemToDto(roomType)
            );
        }

        // PUT: api/TodoItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRoomType(long id, RoomTypesDto roomTypesDto)
        {
            if (id != roomTypesDto.Id)
            {
                return BadRequest();
            }

            var room = await _context.RoomTypes.FindAsync(id);
            if (room == null)
            {
                return NotFound();
            }

            room.Description = roomTypesDto.Description;

            _context.Entry(room).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!RoomTypesExists(id))
            {
                return NotFound();
            }

            return CreatedAtAction(
                nameof(GetRoomType),
                new { id = room.Id },
                ItemToDto(room)
            );
        }

        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRoomType(long id)
        {
            if (_context.RoomTypes == null)
            {
                return NotFound();
            }

            var room = await _context.RoomTypes.FindAsync(id);
            if (room == null)
            {
                return NotFound();
            }

            _context.RoomTypes.Remove(room);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool RoomTypesExists(long id)
        {
            return (_context.RoomTypes?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        private static RoomTypesDto ItemToDto(RoomTypes room) =>
            new RoomTypesDto
            {
                Id = room.Id,
                Description = room.Description,
            };
    }
}