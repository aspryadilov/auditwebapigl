using Microsoft.EntityFrameworkCore;
using AuditAPI.Models;

namespace AuditAPI.Contexts
{
    public class AuditContext : DbContext
    {
        public AuditContext(DbContextOptions<AuditContext> options)
            : base(options)
        {
        }

        public DbSet<BuildingItem> BuildingItems { get; set; } = null!;
        public DbSet<Departments> Departments { get; set; } = null!;
        public DbSet<RoomTypes> RoomTypes { get; set; } = null!;
        public DbSet<Appointments> Appointments { get; set; } = null!;
        public DbSet<Rooms> Rooms { get; set; } = null!;
        public DbSet<FileItem> File { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rooms>()
                .HasOne(r => r.Appointment)
                .WithOne(a => a.Room)
                .HasForeignKey<Appointments>(a => a.RoomId);
            modelBuilder.Entity<Rooms>()
                .HasOne(r => r.RoomType)
                .WithOne(rt => rt.Room)
                .HasForeignKey<RoomTypes>(rt => rt.RoomId);

            modelBuilder.Entity<Rooms>()
                .HasOne(r => r.Department)
                .WithOne(d => d.Room)
                .HasForeignKey<Departments>(d => d.RoomId);

            modelBuilder.Entity<Rooms>()
                .HasOne(r => r.BuildingItem)
                .WithMany(bi => bi.Rooms)
                .HasForeignKey(bi => bi.BuildingItemId);
        }
    }
}