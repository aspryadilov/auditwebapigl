using System.ComponentModel.DataAnnotations;

namespace AuditAPI.Models
{
    public class BuildingItem
    {
        [Key]
        public long Id { get; set; }
        public string? Name { get; set; } = string.Empty;
        public string? Address { get; set; } = string.Empty;
        public string? NumOfStoreys { get; set; } = string.Empty;
        public List<Rooms> Rooms { get; set; }
    }
}