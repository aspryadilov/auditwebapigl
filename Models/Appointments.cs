namespace AuditAPI.Models;

public class Appointments
{
    public long Id { get; set; }
    public string? Description { get; set; } = string.Empty;
    public Rooms? Room { get; set; }
    public long? RoomId { get; set; }
}