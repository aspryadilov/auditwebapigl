﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AuditAPI.Migrations
{
    /// <inheritdoc />
    public partial class newtest1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rooms_BuildingItems_BuildingItemId1",
                table: "Rooms");

            migrationBuilder.DropIndex(
                name: "IX_Rooms_BuildingItemId1",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "BuildingItemId1",
                table: "Rooms");

            migrationBuilder.AlterColumn<long>(
                name: "BuildingItemId",
                table: "Rooms",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rooms_BuildingItemId",
                table: "Rooms",
                column: "BuildingItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_Rooms_BuildingItems_BuildingItemId",
                table: "Rooms",
                column: "BuildingItemId",
                principalTable: "BuildingItems",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rooms_BuildingItems_BuildingItemId",
                table: "Rooms");

            migrationBuilder.DropIndex(
                name: "IX_Rooms_BuildingItemId",
                table: "Rooms");

            migrationBuilder.AlterColumn<int>(
                name: "BuildingItemId",
                table: "Rooms",
                type: "integer",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "BuildingItemId1",
                table: "Rooms",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rooms_BuildingItemId1",
                table: "Rooms",
                column: "BuildingItemId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Rooms_BuildingItems_BuildingItemId1",
                table: "Rooms",
                column: "BuildingItemId1",
                principalTable: "BuildingItems",
                principalColumn: "Id");
        }
    }
}
