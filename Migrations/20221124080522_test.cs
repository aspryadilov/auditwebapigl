﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AuditAPI.Migrations
{
    /// <inheritdoc />
    public partial class test : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NumFloors",
                table: "Rooms",
                newName: "Floors");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Floors",
                table: "Rooms",
                newName: "NumFloors");
        }
    }
}
