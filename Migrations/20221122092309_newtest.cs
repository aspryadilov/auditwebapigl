﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AuditAPI.Migrations
{
    /// <inheritdoc />
    public partial class newtest : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "RoomId",
                table: "RoomTypes",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BuildingItemId",
                table: "Rooms",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "BuildingItemId1",
                table: "Rooms",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DepartmentId",
                table: "Rooms",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RoomTypesId",
                table: "Rooms",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RoomId",
                table: "Departments",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoomTypes_RoomId",
                table: "RoomTypes",
                column: "RoomId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rooms_BuildingItemId1",
                table: "Rooms",
                column: "BuildingItemId1");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_RoomId",
                table: "Departments",
                column: "RoomId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Departments_Rooms_RoomId",
                table: "Departments",
                column: "RoomId",
                principalTable: "Rooms",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Rooms_BuildingItems_BuildingItemId1",
                table: "Rooms",
                column: "BuildingItemId1",
                principalTable: "BuildingItems",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomTypes_Rooms_RoomId",
                table: "RoomTypes",
                column: "RoomId",
                principalTable: "Rooms",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Departments_Rooms_RoomId",
                table: "Departments");

            migrationBuilder.DropForeignKey(
                name: "FK_Rooms_BuildingItems_BuildingItemId1",
                table: "Rooms");

            migrationBuilder.DropForeignKey(
                name: "FK_RoomTypes_Rooms_RoomId",
                table: "RoomTypes");

            migrationBuilder.DropIndex(
                name: "IX_RoomTypes_RoomId",
                table: "RoomTypes");

            migrationBuilder.DropIndex(
                name: "IX_Rooms_BuildingItemId1",
                table: "Rooms");

            migrationBuilder.DropIndex(
                name: "IX_Departments_RoomId",
                table: "Departments");

            migrationBuilder.DropColumn(
                name: "RoomId",
                table: "RoomTypes");

            migrationBuilder.DropColumn(
                name: "BuildingItemId",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "BuildingItemId1",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "RoomTypesId",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "RoomId",
                table: "Departments");
        }
    }
}
