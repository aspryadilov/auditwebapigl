﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace AuditAPI.Migrations
{
    /// <inheritdoc />
    public partial class newtest12 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Plan",
                table: "Rooms");

            migrationBuilder.AddColumn<long>(
                name: "FileId",
                table: "Rooms",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "File",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Path = table.Column<string>(type: "text", nullable: true),
                    RoomId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_File", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Rooms_FileId",
                table: "Rooms",
                column: "FileId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Rooms_File_FileId",
                table: "Rooms",
                column: "FileId",
                principalTable: "File",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rooms_File_FileId",
                table: "Rooms");

            migrationBuilder.DropTable(
                name: "File");

            migrationBuilder.DropIndex(
                name: "IX_Rooms_FileId",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "FileId",
                table: "Rooms");

            migrationBuilder.AddColumn<string>(
                name: "Plan",
                table: "Rooms",
                type: "text",
                nullable: true);
        }
    }
}
