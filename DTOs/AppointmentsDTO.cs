using AuditAPI.Models;

namespace AuditAPI.DTOs;

public class AppointmentsDto
{
    public long Id { get; set; }
    public string? Description { get; set; } = string.Empty;
    public Rooms? Room { get; set; }
    public long? RoomId { get; set; } 
}