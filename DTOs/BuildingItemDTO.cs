namespace AuditAPI.DTOs
{
    public class BuildingItemDto
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public string? Address { get; set; }
        public string? NumOfStoreys { get; set; }
    }
}