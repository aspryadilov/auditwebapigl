using AuditAPI.Models;

namespace AuditAPI.DTOs;

public class DepartmentsDto
{
    public long Id { get; set; }
    public string? Name { get; set; } = string.Empty;
    public Rooms? Room { get; set; }
    public long? RoomId { get; set; }
}