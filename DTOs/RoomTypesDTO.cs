using AuditAPI.Models;

namespace AuditAPI.DTOs;

public class RoomTypesDto
{
    public long Id { get; set; }
    public string? Description { get; set; }
    public Rooms? Room { get; set; }
    public long? RoomId { get; set; } 
}