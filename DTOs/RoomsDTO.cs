using AuditAPI.Models;

namespace AuditAPI.DTOs;

public class RoomsDTO
{
    public long Id { get; set; }
    public string? Name { get; set; } = string.Empty;
    public long? AppointmentId { get; set; }
    public Appointments? Appointment { get; set; }
    public long? RoomTypesId { get; set; }
    public RoomTypes? RoomType { get; set; }
    public long? DepartmentId { get; set; }
    public long? BuildingItemId { get; set; }
    public string? Square { get; set; } = string.Empty;
    public long? FileId { get; set; }
    public FileItem? File { get; set; }
    public int? Capacity { get; set; }
    public int? Floors { get; set; }
    public string? Number { get; set; } = string.Empty;
}