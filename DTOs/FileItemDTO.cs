using AuditAPI.Models;

namespace AuditAPI.DTOs;

public class FileItemDto
{
    public long Id { get; set; }
    public string? Name { get; set; }
    public string? Path { get; set; }
    public long? RoomId { get; set; }
    public Rooms? Rooms { get; set; }
}   